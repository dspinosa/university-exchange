from typing import Optional

from sqlalchemy import orm
from pydantic import BaseModel, EmailStr
from typing import Optional
from datetime import datetime


class StudentBase(BaseModel):
    name: str
    username: str


class Student(BaseModel):
    id: int
    username: str
    name: str
    email: EmailStr
    hashed_password: str

    class Config:
        orm_mode = True


class StudentCreate(StudentBase):
    email: str
    hashed_password: str


class StudentUpdate(StudentBase):
    hashed_password: Optional[str] = None


class UniversityCreate(BaseModel):
    name: str
    city: str
    country: str


class University(BaseModel):
    id: int
    name: str
    city: str
    country: str

    class Config:
        orm_mode = True


class CareerBase(BaseModel):
    name: str


class Career(CareerBase):
    owner_univ_id: int

    class Config:
        orm_mode = True


class CareerCreate(CareerBase):
    pass


class BookBase(BaseModel):
    name: str
    title: str
    description: str
    date: Optional[datetime] = None


class BookCreate(BookBase):
    owner_id: int
    price: float
    career_id: int


class Book(BookBase):
    owner_id: int
    price: float
    career_id: int

    class Config:
        orm_mode = True


class BookUpdate(BookBase):
    price: float


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenPayload(BaseModel):
    sub: Optional[int] = None
