# app/main.py

from fastapi import FastAPI, Depends, HTTPException
from . import crud, models, schemas
from .api.v1 import login
from sqlalchemy.orm import Session
from .db.database import SessionLocal, engine

app = FastAPI(title="FastAPI, Docker, and Traefik")

from .routers import books, careers, students

app.include_router(books.router)
app.include_router(careers.router)
app.include_router(students.router)
app.include_router(login.router)


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# @app.post("/students22/", response_model=schemas.Student)
# def create_student(student: schemas.StudentCreate, db: Session = Depends(get_db)):
#     db_student = crud.get_student_by_email(db, email=student.email)
#     if db_student:
#         raise HTTPException(status_code=400, detail="Student already registered")
#     return crud.create_student(db=db, student=student)

# @app.get("/students/")
# def get_students(db: Session = Depends(get_db)):
#     return crud.get_students(db=db)

# @app.get("/student/{student_id}", response_model=schemas.Student)
# def get_student(student_id: int, db: Session = Depends(get_db)):
#     return crud.get_student(db=db, id=student_id)

@app.post("/university/", response_model=schemas.University)
def create_university(university: schemas.UniversityCreate, db: Session = Depends(get_db)):
    return crud.create_university(db=db, university=university)

@app.get("/university/{university_id}", response_model=schemas.University)
def get_university(university_id: int, db: Session = Depends(get_db)):
    return crud.get_university(db=db, id=university_id)

@app.post("/universities/{university_id}/careers/", response_model=schemas.Career)
def create_career_for_university(university_id: int, career: schemas.CareerCreate, db: Session = Depends(get_db)):
    return crud.create_career(db=db, career=career, university_id=university_id)

#@app.get("/career/{career_id}", response_model=schemas.Career)
#def get_career(career_id: int, db: Session = Depends(get_db)):
    #return crud.get_career(db=db, id=career_id)

@app.post("/student/{student_id}/book/", response_model=schemas.Book)
def create_book_for_student(student_id: int, book: schemas.BookCreate, db: Session = Depends(get_db)):
    return crud.create_book(db=db, book=book, student_id=student_id)

