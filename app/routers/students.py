from fastapi import APIRouter, Depends, HTTPException
from ..db.database import SessionLocal
from sqlalchemy.orm import Session
from ..schemas import Student, StudentCreate, StudentUpdate
from ..crud.students import *
from ..api.dependencies import get_db, oauth2_scheme
from fastapi.security import OAuth2PasswordBearer

router = APIRouter(
    prefix="/students",
    tags=["students"],
    responses={404: {"description": "Not Found."}}
)


@router.get("/")
def students(db: Session = Depends(get_db)):
    return get_students(db=db)


@router.get("/{student_id}", response_model=Student)
def get_student_by_id(student_id: int, db: Session = Depends(get_db)):
    return get_student(db=db, id=student_id)


@router.post("/", response_model=Student)
def student(st: StudentCreate, db: Session = Depends(get_db)):
    db_student = get_student_by_email(db, email=st.email)
    if db_student:
        raise HTTPException(status_code=400, detail="Student already registered")
    return create(db=db, student_in=st)


@router.put("/{student_id}", response_model=Student)
def student_update(student_id: int, student: StudentUpdate, db: Session = Depends(get_db)):
    return update_students(db=db, id=student_id, student=student)


@router.delete("/{student_id}")
def delete_student(student_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    return delete_students(db=db, id=student_id)
