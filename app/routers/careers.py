from fastapi import APIRouter, Depends
from ..db.database import SessionLocal
from sqlalchemy.orm import Session
from ..schemas import Career
from ..api.dependencies import get_db
from ..crud.careers import get_career, get_careers


router = APIRouter(
    prefix="/careers",
    tags=["careers"],
    responses={404: {"description": "Not Found."}}
)


@router.get("/")
def get_list(db: Session = Depends(get_db)):
    return get_careers(db=db)


@router.get("/{career_id}")
def get(career_id: int, db: Session = Depends(get_db)):
    return get_career(db=db, id=career_id)

