from fastapi import Depends, APIRouter
from typing import List
from ..api.dependencies import get_db, oauth2_scheme
from sqlalchemy.orm import Session
from ..schemas import Book, BookCreate, BookUpdate
from ..crud.books import *

router = APIRouter(
    prefix="/books",
    tags=["books"],
    responses={404:{"description": "Not Found"}}
)


@router.post("/")
def create_book_for_student(book: BookCreate, db: Session = Depends(get_db)):
    return create_book(db=db, book=book)


@router.get("/", response_model=List[Book])
def get_books_list(db: Session = Depends(get_db)):
    return get_books(db=db, limit=50)


@router.get("/{book_id}", response_model=Book)
def get_book_by_id(book_id: int, db: Session = Depends(get_db)):
    return get_book(db=db, id=book_id)


@router.put("/{book_id}", response_model=Book)
def update(book_id: int, book: BookUpdate, db: Session = Depends(get_db)):
    return update_book(db=db, id=book_id, book=book)


@router.delete("/{book_id}")
def delete(book_id: int, db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    return delete_book(db=db, id=book_id)
