from ..db.database import SessionLocal
from fastapi.security import OAuth2PasswordBearer


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def oauth2_scheme():
    return OAuth2PasswordBearer(tokenUrl='/login/access-token')
