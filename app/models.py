from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import DateTime, Float

from .db.database import Base


class Student(Base):
    __tablename__ = "students"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    name = Column(String, index=True)
    email = Column(String, unique=True, index=True)
    is_active = Column(Boolean, default=True)
    is_superuser = Column(Boolean, default=False)
    hashed_password = Column(String)
    books = relationship("Book", back_populates="owner")


class Book(Base):
    __tablename__ = "books"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    title = Column(String)
    description = Column(String)
    price = Column(Float)
    career_id = Column(Integer, ForeignKey("careers.id"))
    date = Column(DateTime)
    owner_id = Column(Integer, ForeignKey("students.id"))
    owner = relationship("Student", back_populates="books")


class University(Base):
    __tablename__ = "universities"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True)
    city = Column(String)
    country = Column(String)

    careers = relationship("Career", back_populates="owner_univ")


class Career(Base):
    __tablename__ = "careers"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    owner_univ_id = Column(Integer, ForeignKey("universities.id"))

    owner_univ = relationship("University", back_populates="careers")

