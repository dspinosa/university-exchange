from sqlalchemy.orm import Session
from .. import models, schemas


def create_career(db: Session, career: schemas.Career, university_id: int):
    db_career = models.Career(**career.dict(), owner_univ_id=university_id)
    db.add(db_career)
    db.commit()
    db.refresh(db_career)
    return db_career


def get_careers(db: Session, skip: int = 0):
    return db.query(models.Career).offset(skip).limit(100).all()


def get_career(db: Session, id: int):
    return db.query(models.Career).filter(models.Career.id == id).first()
