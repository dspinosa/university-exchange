from sqlalchemy.orm import Session
from .. import models, schemas


def create_university(db: Session, university: schemas.UniversityCreate):
    db_university = models.University(
        name=university.name,
        city=university.city,
        country=university.country
    )
    db.add(db_university)
    db.commit()
    db.refresh(db_university)
    return db_university


def get_university(db: Session, id: int):
    return db.query(models.University).filter(models.University.id == id).first()
