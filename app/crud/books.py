from sqlalchemy.orm import Session
from .. import models, schemas
from datetime import date


def create_book(db: Session, book: schemas.BookCreate):
    db_student = models.Book(**book.dict())
    db.add(db_student)
    db.commit()
    db.refresh(db_student)
    return db_student


def get_book(db: Session, id: int):
    return db.query(models.Book).filter(models.Book.id == id).first()


def get_books(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Book).offset(skip).limit(limit).all()


def update_book(db: Session, id: int, book: schemas.BookUpdate):
    db_book = db.query(models.Book).filter(models.Book.id == id).first()
    db_book.name = book.name
    db_book.title = book.title
    db_book.description = book.description
    db_book.price = book.price
    db_book.date = date.today()
    db.commit()
    db.refresh(db_book)
    return db_book


def delete_book(db: Session, id: int):
    db_book = db.query(models.Book).filter(models.Book.id == id).first()
    db.delete(db_book)
    db.commit()
