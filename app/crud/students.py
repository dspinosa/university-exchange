from sqlalchemy.orm import Session
from .. import models, schemas
from typing import Optional
from ..core.security import get_password_hash, verify_password


def create(db: Session, student_in: schemas.StudentCreate) -> models.Student:
    db_student = models.Student(
        username=student_in.username,
        name=student_in.name,
        email=student_in.email,
        hashed_password=get_password_hash(student_in.hashed_password)
    )
    db.add(db_student)
    db.commit()
    db.refresh(db_student)
    return db_student


def get_students(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Student).offset(skip).limit(100).all()


def get_student_by_email(db: Session, email: str):
    return db.query(models.Student).filter(models.Student.email == email).first()


def get_student(db: Session, id: int):
    return db.query(models.Student).filter(models.Student.id == id).first()


def update_students(db: Session, id: int, student: schemas.StudentUpdate):
    student_db = db.query(models.Student).filter(models.Student.id == id).first()
    if isinstance(student, dict):
        update_data = student
        breakpoint()
    student_db.name = student.name
    student_db.username = student.username
    if student.hashed_password:
        student_db.hashed_password = get_password_hash(student.hashed_password)
    db.commit()
    db.refresh(student_db)
    return student_db


def delete_students(db: Session, id: int):
    student_db = db.query(models.Student).filter(models.Student.id == id).first()
    db.delete(student_db)
    db.commit()


def authenticate(db: Session,
                 *,
                 email: str,
                 password: str) -> Optional[models.Student]:
    student = get_student_by_email(db, email=email)
    if not student:
        return None
    if not verify_password(password, student.hashed_password):
        return None
    return student


def is_active(student: models.Student) -> bool:
    return student.is_active

# def is_superuser(self, user: User) -> bool:
#     return user.is_superuser

