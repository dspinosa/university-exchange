"""initial2

Revision ID: 1200afce3315
Revises: 6bb26d9c8070
Create Date: 2021-09-20 19:48:10.954505

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1200afce3315'
down_revision = '6bb26d9c8070'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
